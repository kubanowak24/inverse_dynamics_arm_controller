function inverse_dynamics_control()
    c = inverse_dynamics_controller_debug(3);
%     c = inverse_dynamics_controller_operational(3);
    
    t = timer('TimerFcn', {@timerCallback, c}, ...
                'Period', 0.05, ...
                'ExecutionMode', 'fixedRate');
    start(t);
    pause
    stop(t);
    delete(t);
    
    c.publish(zeros(6, 1), zeros(3, 1));
    
    pause(0.1);
    rosshutdown;
end

function timerCallback(timer, ~, c)
    tau = c.u(timer.Period);
    c.publish(tau, c.v);
end
