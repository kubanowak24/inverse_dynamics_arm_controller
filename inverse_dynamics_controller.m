classdef inverse_dynamics_controller < handle
   properties
       q_sub = [];
       qdot_sub = [];
       tau_sub = [];
       tau_meas = zeros(8, 1);
       q_d_sub = [];
       q_ddot_sub = [];
       q_dddot_sub = [];
       tau_pub = [];
       tau_msg = [];
       tau_pub_cells = cell(4);
       tau_msg_cells = cell(4);
       q = [];
       qdot = [];
       prev_q = [];
       
       q_d = [];
       q_dot_d = zeros(3, 1);
       q_ddot_d = zeros(3, 1);
       
%        Kp = diag([30.0 30.0 60.0]) * 0.01;
       Kp = diag([1 1 1]) * 0.25;
       Kv = diag([1.0 1.0 1.0]) * 0.1;
       Ki = diag([1 1 1])*0.0;
       i = 0;
       imax = 2;
       
       v = zeros(3, 1);
       v_pub = [];
       v_msg = [];
       v_data = [];
   end
   
   methods
       function obj = inverse_dynamics_controller(i)
           rosshutdown;
           rosinit('12.0.10.112');
           
           obj.q_sub = rossubscriber(sprintf('/widowx_arm%d/joints_poses', i), 'std_msgs/Float32MultiArray', @obj.q_callback);
           obj.qdot_sub = rossubscriber(sprintf('/widowx_arm%d/joints_vels', i), 'std_msgs/Float32MultiArray', @obj.qdot_callback);
           obj.tau_sub = rossubscriber(sprintf('/widowx_arm%d/joints_torques', i), 'std_msgs/Float32MultiArray', @obj.tau_callback);
%            obj.q_sub = rossubscriber(sprintf('/nexus_arm0/joints_poses', i), 'std_msgs/Float32MultiArray', @obj.q_callback);
%            obj.qdot_sub = rossubscriber(sprintf('/nexus_arm0/joints_vels', i), 'std_msgs/Float32MultiArray', @obj.qdot_callback);
%            obj.tau_sub = rossubscriber(sprintf('/nexus_arm0/joints_torques', i), 'std_msgs/Float32MultiArray', @obj.tau_callback);
           
           obj.q_d_sub = rossubscriber('/position_setpoint', 'std_msgs/Float32MultiArray', @obj.q_d_callback);
           obj.q_ddot_sub = rossubscriber('/velocity_setpoint', 'std_msgs/Float32MultiArray', @obj.q_ddot_callback);
           obj.q_dddot_sub = rossubscriber('/acceleration_setpoint', 'std_msgs/Float32MultiArray', @obj.q_dddot_callback);
          
           [obj.tau_pub, obj.tau_msg] = rospublisher(sprintf('/widowx_arm%d/torque_setpoint', i), 'std_msgs/Float32MultiArray');
           [obj.v_pub, obj.v_msg] = rospublisher(sprintf('/widowx_arm%d/velocity_setpoint', i), 'std_msgs/Float32MultiArray');
%            [obj.tau_pub, obj.tau_msg] = rospublisher(sprintf('/nexus_arm0/torque_setpoint', i), 'std_msgs/Float32MultiArray');
%            [obj.v_pub, obj.v_msg] = rospublisher(sprintf('/nexus_arm0/velocity_setpoint', i), 'std_msgs/Float32MultiArray');

       end
       
       function q_callback(obj, ~, msg)
           obj.q = [0; 0; 0; msg.Data(1:end-1)];
       end
       
       function qdot_callback(obj, ~, msg)
           VELOCITY_SCALING = 41.7014;
           obj.qdot = [0; 0; 0; msg.Data(1:end-1) / VELOCITY_SCALING; 0];
       end
       
       function tau_callback(obj, ~, msg)
           t = msg.Data;
           obj.tau_meas = zeros(5,1);
           obj.tau_meas(1) = t(1);
           obj.tau_meas(2) = t(2) - t(3);
           obj.tau_meas(3) = t(4) - t(5);
           obj.tau_meas(4) = -t(6);
           obj.tau_meas(5) = 0;
       end
       
       function q_d_callback(obj, ~, msg)
           obj.q_d = msg.Data;
       end
       
       function q_ddot_callback(obj, ~, msg)
           obj.q_ddot_d = msg.Data;
       end
       
       function q_dddot_callback(obj, ~, msg)
           obj.q_dot_d = msg.Data;
       end
       
       function val = k(~, q)
           val = zeros(6, 1);
           val(1:3) = position_EE(q(4), q(5), q(6), q(7));
           rot = rotation_matrix_EE(q(4), q(5), q(6), q(7), q(8));
           val(4:6) = rotm2eul(rot);
       end
       
       function tau = u(obj, dt)
           if isempty(obj.q) || isempty(obj.qdot) || isempty(obj.q_d)
               tau = zeros(8, 1);
               return
           end
           
           % only use part of the model:
           q_ = obj.q(5:end-1);
           qdot_ = obj.qdot(5:end-1);
           J = Jacobian_EE(0, obj.q(4), obj.q(5), obj.q(6), obj.q(7), 0);
           J = J(:, [1:2 4:end]);
           
           % only use part of the model:
           J = J([1 3 5], 5:end-1);
           
           xdot = J * qdot_;
           x = obj.k(obj.q);
           x = x([1 3 5]);

%            x_d = obj.k([0 0 0 0 obj.q_d(1) obj.q_d(2) obj.q_d(3) 0 ]);
%            x_d = x_d([1 3 5]);
           x_d = obj.q_d;
%            u = obj.Kp * (x_d - x) - obj.Kv * xdot;
           u = obj.Kp * (x_d - x);
           tau = J.' * u;
           
           TORQUE_LIMIT = 3;
           tau(tau>TORQUE_LIMIT) = TORQUE_LIMIT;
           tau(tau<-TORQUE_LIMIT) = -TORQUE_LIMIT;
       end
       
       function publish(obj, tau, v)
           msg_data = [0; tau; 0];
           msg_data(2) = msg_data(2)/2;
           msg_data(3) = msg_data(3)/2;
           obj.tau_msg.Data = msg_data;
           send(obj.tau_pub, obj.tau_msg);
%            obj.v_data = [0; v; 0];
%            obj.v_msg.Data = obj.v_data;
%            send(obj.v_pub, obj.v_msg);
       end
   end
end
